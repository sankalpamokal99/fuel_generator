import "./App.css";

function App() {
  return (
    <>
      <div className="main-container">
        <div className="bill-container">
          <div className="logo"></div>
          <div className="bill-body">
            <div className="header-grp">
              <div className="name">JF PANKI</div>
              <div className="address">
                AMBEGHAR,PEN
                <br />
                DIST-RAIGAD(MH)
              </div>
              <div className="number">PH-9323827777</div>
            </div>
            <div className="details-grp">
              <div className="detail-row">
                <div className="label">
                  Bill No<span>:</span>
                </div>
                <div className="value">697874-ORGNL</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Trns.ID<span>:</span>
                </div>
                <div className="value">0000000300987896</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Atnd.ID<span>:</span>
                </div>
                <div className="value"></div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Receipt<span>:</span>
                </div>
                <div className="value">Physical Receipt</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Vehi.No<span>:</span>
                </div>
                <div className="value">MH06CL1128</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Mob.No<span>:</span>
                </div>
                <div className="value">NotEntered</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Date<span>:</span>
                </div>
                <div className="value">16/06/2024</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Time<span>:</span>
                </div>
                <div className="value">14:38:52</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  FP.ID<span>:</span>
                </div>
                <div className="value">2</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Nozl No<span>:</span>
                </div>
                <div className="value">2</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Fuel<span>:</span>
                </div>
                <div className="value">PETROL</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Density<span>:</span>
                </div>
                <div className="value">75.18kg/m3</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Preset<span>:</span>
                </div>
                <div className="value">NON PRESET</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Rate<span>:</span>
                </div>
                <div className="value">Rs.104.88</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Sale<span>:</span>
                </div>
                <div className="value">Rs.3512.23</div>
              </div>
              <div className="detail-row">
                <div className="label">
                  Volume<span>:</span>
                </div>
                <div className="value">33.49L</div>
              </div>
              <div className="note">THANK YOU, VISIT AGAIN.</div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
